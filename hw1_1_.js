"use strict"

//Теоретический вопрос
//1. Переменные var могут объявляться еще раз или обновляться. Переменная var является глобальной, когда объявлена вне какой-либо функции и является локальной, когда объявлена внутри функции. Переменные let видны только после объявления и только в текущем блоке и их нельзя переобъявлять в том же блоке. При объявлении переменной в цикле она видна только в этом цикле. Причём каждой итерации соответствует своя переменная let. Переменная const нельзя менять.
//2. Область видимости переменных var слишком большой. Это может привести к ошибкам. 

// Задание

let userName = prompt("Enter your name")
let userAge = +prompt("Enter your age")

if (userAge < 18) {
  alert ("You are not allowed to visit this website") 
}


else if(userAge >= 18 && userAge <= 22) {  
    let ageAnswer = confirm ("Are you sure you want to continue?")
    if (ageAnswer == true) {
        alert ("Welcome, " + userName)
    }
    else {
        alert ("You are not allowed to visit this website")
    }
}

else {
    alert ("Welcome, " + userName)
}